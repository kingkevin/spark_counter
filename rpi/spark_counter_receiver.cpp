/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2015 King Kévin <kingkevin@cuvoodoo.info> */
/* the spark counter receives electricity measurements over radio
 * they are then received using an nRF2L01+ transceiver
 * to communicate with the transceiver the RF24 library is requiered
 * they are then stored in an InfluxDB time series database using the HTTP API
 * to send values to the database the curl library is required
 */
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <RF24/RF24.h> // http://tmrh20.github.io/RF24 library to communicate to the nRF24L01+
#include <curl/curl.h> // curl library to send the measurement data to the influxDB.
#include "aes.h" // AES library (from tiny-AES128-C)
#include "crc16.h" // CRC Computations (from AVR libc)

//#define DEBUG 1

// Setup for RPi B1 GPIO 22 CE and CE0 CSN with SPI Speed @ 8Mhz
RF24 radio(RPI_V2_GPIO_P1_22, BCM2835_SPI_CS0, BCM2835_SPI_SPEED_8MHZ);

// nRF24L01+ addresses
const uint8_t tx_addr[] = {1,'h','o','m','e'};
const uint8_t rx_addr[] = {0,'h','o','m','e'};

CURL *curl; // curl handle to post data to influxbd using the HTTP API

// key material
const uint8_t key[16] = {0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff};
uint8_t iv[16] = {0xff,0xee,0xdd,0xcc,0xbb,0xaa,0x99,0x88,0x77,0x66,0x55,0x44,0x33,0x22,0x11,0x00};

int main(int argc, char** argv){

  if (argc!=4) {
    fprintf(stderr, "provide database name, username, and password as argument (spaces, ':', special characters, or very long arguments not supported)\n");
    return 1;
  }

  // configure influxdb connection
  curl = curl_easy_init();
  if (curl) {
    char url[256];
    snprintf(url, sizeof(url), "http://localhost:8086/write?db=%s", argv[1]);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    char auth[256];
    snprintf(auth, sizeof(auth), "%s:%s", argv[2], argv[3]);
    curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_easy_setopt(curl, CURLOPT_USERPWD, auth);
  } else {
    fprintf(stderr, "could not initialize curl\n");
    return 1;
  }

  // configure nRF24L01+ radio
  radio.begin();
  radio.setChannel(42);
  radio.setPALevel(RF24_PA_MAX);
  radio.setDataRate(RF24_1MBPS);
  radio.setAutoAck(true);
  radio.enableDynamicPayloads();
  radio.setRetries(2,15);
  radio.setCRCLength(RF24_CRC_8);
  radio.openWritingPipe(tx_addr);
  radio.openReadingPipe(1,rx_addr);
  //radio.printDetails();
  
  printf("wait for packet to arrive\n");
  radio.startListening();
  while (1) { // forever loop
    while (radio.available()) {
      time_t ltime; // calendar time 
      ltime=time(NULL); // get current cal time
      char stime[64]; // do display the time
      strftime(stime, sizeof(stime), "%F %T %z %Z", localtime(&ltime));
      printf("%s: payload received over radio\n",stime);
      uint8_t payload[32]; // buffer to save the payload
      uint8_t size = radio.getDynamicPayloadSize();
      radio.read(&payload,size);

#ifdef DEBUG
      printf("got %d bytes:",size);
      for (uint8_t i=0; i<size; i++) {
        printf(" %02x",payload[i]);
      }
      printf("\n");
#endif

      // got through payload
      if (size!=1+4*4+1) {
#ifdef DEBUG
		printf("wrong size: expected %d, got %d\n",18,size);
#endif
        continue;
      }
      uint8_t id = payload[0]; // the meter id (0 is myself, used for unknown source)
      if (id!=1) { // there is only one source for power measurement
#ifdef DEBUG
        printf("wrong id: expected %d, got %d\n",1,id);
#endif
        continue;
      }
      uint8_t values[16]; // the encrypted values block
      AES128_ECB_decrypt(&payload[1], key, values); // decrypt values
#ifdef DEBUG
      printf("decrypted values:");
      for (uint8_t i=0; i<sizeof(values); i++) {
        printf(" %02x",values[i]);
      }
      printf("\n");
#endif
      for (uint8_t i=0; i<sizeof(values) && i<sizeof(iv); i++) { // use CBC mode
        values[i] ^= iv[i]; // XOR with last IV
      }
#ifdef DEBUG
      printf("XORed values:");
      for (uint8_t i=0; i<sizeof(values); i++) {
        printf(" %02x",values[i]);
      }
      printf("\n");
#endif
      memcpy(iv,&payload[1],sizeof(iv)); // save next IV
      // check CRC of plain values
      uint8_t crc = 0;
      for (uint8_t i = 0; i < sizeof(values); i++) {
        crc = _crc_ibutton_update(crc, values[i]); // calculate CRC
      }
      crc = _crc_ibutton_update(crc, payload[17]); // this is the CRC
      if (crc) { // wrong key or IV
        printf("wrong CRC. saving IV for next packet\n");
        continue;
      }
      uint8_t invalid[sizeof(values)]; // used to indicate that the values are invalid
      memset(invalid,0xff,sizeof(invalid)); // 0xff... is the invalid value
      if (memcmp(invalid,values,sizeof(values))==0) { // values are invalid
#ifdef DEBUG
        printf("invalid values\n");
#endif
        continue;
      }
      // read values
      float voltage, current, power, energy; // the values coming from the meter
      memcpy(&voltage,&values[0],4); // read voltage
      memcpy(&current,&values[4],4); // read current
      memcpy(&power,&values[8],4); // read power
      memcpy(&energy,&values[12],4); // read energy
      printf("meter: %d, voltage: %f V, current: %f A, power: %f W, energy: %f Wh\n",id,voltage,current,power,energy);
      // submit values to database
      if (curl) {
        CURLcode res = CURLE_OK; // curl response
        char post[128*4] = {0}; // string to submit data to DB using POST request
        snprintf(post, sizeof(post), "voltage,meter=%d value=%f\ncurrent,meter=%d value=%f\npower,meter=%d value=%f\nenergy,meter=%d value=%f\n", id, voltage, id, current, id, power, id, energy);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post);
        res = curl_easy_perform(curl);
        if (res!= CURLE_OK) {
          fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        }
      }
    }
    usleep(100000); // wait 100ms before request if data is available again (the IRQ signal is not used)
  }

  if (curl) {
    curl_easy_cleanup(curl);
  }
}
