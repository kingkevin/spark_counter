#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 1.9.3
=begin
script to queries the measurements from a peacefair PZEM-004 power meter
the command and data payload are copied from the manual
here the device address is set to 192.168.1.2 in all messages
=end
require 'serialport'

@serial = SerialPort.open("/dev/ttyUSB0",{ baud: 9600, databits: 8, parity: SerialPort::NONE, stop_bit: 1, flow_control: SerialPort::NONE})
@serial.baud = 9600

# set address
cmd = [0xB4,0xC0,0xA8,0x01,0x02,0x00,0x1F]
@serial.write(cmd.pack("C*"))
puts "< "+cmd.collect { |b| sprintf("%02X ",b) }.join
data = @serial.read(7).unpack("C*")
puts "> "+data.collect { |b| sprintf("%02X ",b) }.join
puts "address set (192.168.0.1)"

# get voltage
cmd = [0xB0,0xC0,0xA8,0x01,0x02,0x00,0x1B]
@serial.write(cmd.pack("C*"))
puts "< "+cmd.collect { |b| sprintf("%02X ",b) }.join
data = @serial.read(7).unpack("C*")
puts "> "+data.collect { |b| sprintf("%02X ",b) }.join
puts "voltage: #{(data[1]<<8)+data[2]}.#{data[3]} V"

# get current
cmd = [0xB1,0xC0,0xA8,0x01,0x02,0x00,0x1C]
@serial.write(cmd.pack("C*"))
puts "< "+cmd.collect { |b| sprintf("%02X ",b) }.join
data = @serial.read(7).unpack("C*")
puts "> "+data.collect { |b| sprintf("%02X ",b) }.join
puts "current: #{(data[1]<<8)+data[2]}.#{data[3]} A"

# get power
cmd = [0xB2,0xC0,0xA8,0x01,0x02,0x00,0x1D]
@serial.write(cmd.pack("C*"))
puts "< "+cmd.collect { |b| sprintf("%02X ",b) }.join
data = @serial.read(7).unpack("C*")
puts "> "+data.collect { |b| sprintf("%02X ",b) }.join
puts "power: #{(data[1]<<8)+data[2]}.#{data[3]} W"

# get energy
cmd = [0xB3,0xC0,0xA8,0x01,0x02,0x00,0x1E]
@serial.write(cmd.pack("C*"))
puts "< "+cmd.collect { |b| sprintf("%02X ",b) }.join
data = @serial.read(7).unpack("C*")
puts "> "+data.collect { |b| sprintf("%02X ",b) }.join
puts "energy: #{(data[1]<<16)+(data[2]<<8)+data[3]} Wh"
