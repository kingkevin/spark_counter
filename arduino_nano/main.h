/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2015 King Kévin <kingkevin@cuvoodoo.info> */
/* the spark counter transmits electricity measurements over radio
 * the electricity measurements (voltage, current, power, energy) are query everry second from a peacefair PZEM-004 power meter
 * they are then transmitted using an nRF2L01+ transceiver
 */
 
/* spark counter configuration */
struct configuration {
	uint8_t id; // the identity of the node
	uint8_t rx_addr[5]; // nRF24 receiving address
	uint8_t tx_addr[5]; // nRF24 transmit address
	uint8_t channel; // nRF24 channel
	uint8_t request_period; // how many seconds between power meter requests (should be > 2 to have enough time to query all values)
	uint8_t key[16]; // the AES 128 bits key
	uint8_t iv[16]; // the initialisation vector for CBC encryption
};
struct configuration conf = {1,{1,'h','o','m','e'},{0,'h','o','m','e'},42,5,
	{0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff},
	{0xff,0xee,0xdd,0xcc,0xbb,0xaa,0x99,0x88,0x77,0x66,0x55,0x44,0x33,0x22,0x11,0x00}
};
