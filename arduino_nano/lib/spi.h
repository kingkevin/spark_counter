/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2015 King Kévin <kingkevin@cuvoodoo.info> */
/* This library handles the Serial Peripheral Interface (SPI) */

/* SPI pins */
#define SPI_PORT PORTB
#define SPI_DDR DDRB
#define SPI_PIN PINB
#define SCK_IO PB5
#define MISO_IO PB4
#define MOSI_IO PB3
#define SS_IO PB2

/* SCK frequency divider (F_osc/<SCK_DIV>, with <SCK_DIV> within 2-128) */
#define SCK_DIV 16

/* initialize SPI */
void spi_init(void);
/* receive SPI byte and transmit next on previous transmit completion when transfer is interrupt based (non-blocking) */
ISR(SPI_STC_vect);
/* transmit and receive data over SPI
 * transmits than receives each byte from <data> for <length> bytes
 * the read bytes are saved back in <data>
 * this function returns only when communication finished
 */
void spi_transfer_blocking(uint8_t* data, size_t length);
/* transmit and receive data over SPI
 * transmits than receives each byte from <data> for <length> bytes
 * the read bytes are saved back in <data>
 * global interrupts are required to be enabled
 * the function returns immediatly, while data is transfered
 * to ensure the transfer is complete, use spi_transfer_wait()
 * if data is already being transfered it will idle until the last transfer is completed
 */
void spi_transfer_nonblocking(uint8_t* data, size_t length);
/* transmit and receive data over SPI
 * transmits than receives each byte from <data> for <length> bytes
 * the read bytes are saved back in <data>
 * global interrupts are required to be enabled
 * the function returns when all the data is transfered
 * while the data is transfered it goes into idle mode
 * if <data> is NULL or <length> is 0 it just ensures the previous transfer is complete
 */
void spi_transfer_wait(uint8_t* data, size_t length);
