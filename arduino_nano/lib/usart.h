/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* Copyright (c) 2015 King Kévin <kingkevin@cuvoodoo.info> */
/* This library handles the USART configuration */

/* use interrupts */
#define USART_INTERRUPT 1
/* set tolerance to 3% to allow 115200 baudrate with 16 MHz clock, else use 9600 for default <2% */
#define BAUD_TOL 3
/* serial baudrate, in bits per second (with 8N1 8 bits, no parity bit, 1 stop bit settings) */
#define BAUD 9600

/* input & output streams */
FILE usart_output;
FILE usart_input;
FILE usart_io;

/* show the user how much received data is ready */
volatile uint8_t usart_incoming; 

/* configure serial port */
void usart_init(void);
/* put character on USART stream (blocking) */
void usart_putchar_blocking(char c, FILE *stream);
/* ensure all data has been transmitted */
void usart_flush();
/* get character from USART stream (blocking) */
char usart_getchar(FILE *stream);
#if defined(USART_INTERRUPT) && (USART_INTERRUPT==1)
/* put character on USART stream (non-blocking using a buffer) */
void usart_putchar_nonblocking(char c, FILE *stream);
#endif
